---
Spike Report
---

Graphics Spike 1
================

Introduction
------------

This Spike was completed in 2017 and the folder structure and repo was
updated in 2018.

Materials are very important in terms of how visuals are displayed in
Unreal, particle systems, environments and intricate meshes all use the
Material system.

Bitbucket Link: <https://bitbucket.org/bSalmon842/graphicsspike1>

Goals
-----

You will create 4 "master" or "base" materials and 10+ material
instances. Create a simple level with a number of spheres floating
around, one for each material instance.

-   A base material (M\_Basic\_PBR) with four
    [parameters](https://docs.unrealengine.com/latest/INT/Engine/Rendering/Materials/MaterialInstances/index.html#instancesandparameters) -
    a Vector parameter for base colour (albedo), a scalar parameter for
    metallic (default 0), a scalar parameter for specular (default 0.5),
    and a scalar parameter for roughness (default 0.25).

    -   3+ material instances, including White, Blue, Red, etc. flat
        colour materials which set the vector parameter, and may touch
        the scalar parameters for artistic purposes.

    -   3+ material instances for "metallic" objects, including a gold,
        silver, and bronze.

        -   You will need a suitable base colour, a metallic of 1.0, and
            then adjust specular and roughness per material instance

        -   Base colour examples can be found in the Unreal Engine
            Documentation.

-   A base material (M\_Translucent) using the Translucent blend mode.
    Same options as the first base material, but with an extra scalar
    parameter for opacity.

    -   2+ material instances with varying levels of opacity.

    -   Use the [Shader Complexity
        view](https://docs.unrealengine.com/latest/INT/Engine/UI/LevelEditor/Viewports/ViewModes/#shadercomplexity)
        to inspect how much of a performance impact such materials may
        have

    -   Also investigate using the TLM Surface translucency lighting
        mode

-   A base material (M\_Masked) using the Masked blend mode.

    -   1+ material instances with suitable textured masks (instead of a
        scalar opacity)

    -   Compare and contrast Masked and Translucent blend modes

-   A base material (M\_Textured) for fully textured objects utilising
    PBR correctly (which is hard, as we don't have an artist we are
    collaborating with)!

    -   Accepts four texture parameters, a base colour map, a PBR map
        (roughness in one channel, metallic in another channel, specular
        in another channel), a normal map, and an AO map.

    -   1+ material instances with textures (you can find the textures
        elsewhere, or you can investigate creating them for certain 3d
        objects using the Substance Painter or Substance Designer
        programs)

    -   Note: Be careful with sRGB vs RGB for imported textures.

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   [GameTextures
    guide](https://www.gametextures.com/using-gametextures-in-unreal-engine-4/)

Tasks undertaken
----------------

-   No specific instructions are needed, the Goals section mostly
    details how the task was carried out.

-   For the Textured section the GameTextures guide was used.

What we found out
-----------------

From this Spike we learned how to create Materials in Unreal Engine 4 as
well as their potential to change how the visuals of a scene are
presented. This Spike also demonstrated how useful Material Instances
are for productivity while also demonstrating the advantages of PBR
Materials. PBR Materials take the light from the environment and use it
to change how the object is presented, while saving resources due to
getting better results with less complexity. Developers like to store
multiple parameters into one texture parameter if possible to save on
draw call and memory allocation.

Open Issues/risks
-----------------

The Textured task was unable to be completed properly; the Textured
Material instead uses 5 parameters in comparison to the 4 required. This
stems from my own lack of knowledge on how Texture Maps work.